# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
# Clear existing data

# Create authors
10.times do
  Author.find_or_create_by(name: Faker::Book.author, dob: Faker::Date.between(from: 100.years.ago, to: 18.years.ago))
end

# Create books
100.times do
  author = Author.all.sample
  Book.find_or_create_by(
    title: Faker::Book.title,
    released_date: Faker::Date.between(from: 30.years.ago, to: Date.today),
    genre: Faker::Book.genre,
    language: Faker::Nation.language,
    author: author
  )
end

puts "DONE"