# Irfan Azhar - Test Altech Omega Andalan

## How to install
1. Clone this repository
```
git clone https://gitlab.com/irfanAzhar1/test_altech.git
```
2. fill .env with .env.example format
```
SMTP_USERNAME=
SMTP_MAIL_FROM=
SMTP_PASSWORD=
SMTP_PORT=587
SMTP_ENDPOINT=
HOST_URL=
REDIS_URL=
```
3. Run `bundle install`
4. Run `rails db:migrate`
5. install redis
6. run redis server

## How to run
1. Run `rails s`
2. Run `bundle exec sidekiq`
3. Open `localhost:3000` in your browser

## How to test
1. Run `rspec`

## How to seed database
1. Run `rails db:seed`

## models

### Author
##### Attributes:
- name: string
- dob: date
- created_at
- updated_at
##### Associations:
- has_many :books

### Book
##### Attributes:
- title: string
- author_id: integer
- created_at
- updated_at
- release_date: date
- genre: string
- language: string
##### Associations:
- belongs_to :author


## Web Pages
- /authors
- /authors/new
- /authors/:id/edit
- /books
- /books/new
- /books/:id/edit
- /books/:id
- /authors/:id
- and other devise pages for authentication


## APIs
- GET /authors
- GET /authors/:id
- POST /authors
- PUT /authors/:id
- DELETE /authors/:id
- GET /books
- GET /books/:id
- POST /books
- PUT /books/:id
- DELETE /books/:id


## Documentation
[Postman Documentation](https://documenter.getpostman.com/view/20281399/2s9YC5xXa8)


## Test Result
![Screenshot 2023-09-14 at 18.35.29.png](Screenshot%202023-09-14%20at%2018.35.29.png)