require 'sidekiq/web'

Rails.application.routes.draw do
  resources :books
  resources :authors
  root 'home#index'

  devise_for :users

  resource :authors
  resource :books

  devise_scope :user do
    get '/users/sign_out' => 'devise/sessions#destroy'
  end

  mount Sidekiq::Web => "/sidekiq"
end
