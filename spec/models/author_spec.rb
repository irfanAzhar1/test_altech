# spec/models/author_spec.rb

require 'rails_helper'

RSpec.describe Author, type: :model do
  describe "validations" do
    it "is valid with a name and dob" do
      author = Author.new(name: "John Doe", dob: Date.new(1980, 1, 1))
      expect(author).to be_valid
    end

    it "is not valid without a name" do
      author = Author.new(dob: Date.new(1980, 1, 1))
      expect(author).not_to be_valid
    end

    it "is not valid without a dob" do
      author = Author.new(name: "John Doe")
      expect(author).not_to be_valid
    end

    it "validates uniqueness of name" do
      existing_author = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      author = Author.new(name: "John Doe", dob: Date.new(1990, 1, 1))
      expect(author).not_to be_valid
    end
  end

  describe "age" do
    it "calculates the author's age correctly" do
      author = Author.new(name: "Jane Doe", dob: Date.new(1985, 12, 31))
      current_year = Time.now.year
      expected_age = current_year - author.dob.year
      expect(author.age).to eq(expected_age)
    end
  end

  describe ".search" do
        it "returns authors matching the query" do
      author1 = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      author2 = Author.create(name: "Jane Smith", dob: Date.new(1990, 1, 1))

      result = Author.search("John", 1, 10, nil, nil)
      expect(result).to include(author1)
      expect(result).not_to include(author2)
    end

    it "returns authors in the specified order" do
      author1 = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      author2 = Author.create(name: "Jane Smith", dob: Date.new(1990, 1, 1))

      result = Author.search("", 1, 10, "name", "asc")
      expect(result.first).to eq(author2)
      expect(result.last).to eq(author1)
    end
  end
end
