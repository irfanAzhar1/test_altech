# spec/models/book_spec.rb

require 'rails_helper'

RSpec.describe Book, type: :model do
  describe "validations" do
    it "is valid with all required attributes" do
      author = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      book = Book.new(
        title: "Sample Book",
        author_id: author.id,
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction",
        language: "English"
      )
      expect(book).to be_valid
    end

    it "is not valid without a title" do
      book = Book.new(
        author_id: 1,
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction",
        language: "English"
      )
      expect(book).not_to be_valid
    end

    it "is not valid without an author_id" do
      book = Book.new(
        title: "Sample Book",
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction",
        language: "English"
      )
      expect(book).not_to be_valid
    end

    it "is not valid without a released_date" do
      book = Book.new(
        title: "Sample Book",
        author_id: 1,
        genre: "Fiction",
        language: "English"
      )
      expect(book).not_to be_valid
    end

    it "is not valid without a genre" do
      book = Book.new(
        title: "Sample Book",
        author_id: 1,
        released_date: Date.new(2022, 1, 1),
        language: "English"
      )
      expect(book).not_to be_valid
    end

    it "is not valid without a language" do
      book = Book.new(
        title: "Sample Book",
        author_id: 1,
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction"
      )
      expect(book).not_to be_valid
    end
  end

  describe ".search" do
    it "returns books matching the query" do
      author = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      book1 = Book.create(
        title: "Sample Book 1",
        author_id: author.id,
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction",
        language: "English"
      )
      book2 = Book.create(
        title: "Sample Book 2",
        author_id: author.id,
        released_date: Date.new(2021, 6, 1),
        genre: "Non-Fiction",
        language: "Spanish"
      )

      result = Book.search("Sample", 1, 10, nil, nil, nil, nil, nil)
      expect(result).to include(book1)
      expect(result).to include(book2)
    end

    it "filters books by min_realised_date" do
      author = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      book1 = Book.create(
        title: "Sample Book 1",
        author_id: author.id,
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction",
        language: "English"
      )
      book2 = Book.create(
        title: "Sample Book 2",
        author_id: author.id,
        released_date: Date.new(2021, 6, 1),
        genre: "Non-Fiction",
        language: "Spanish"
      )

      result = Book.search("Sample", 1, 10, Date.new(2022, 1, 1), nil, nil, nil, nil)
      expect(result).to include(book1)
      expect(result).not_to include(book2)
    end

    it "filters books by max_realised_date" do
      author = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      book1 = Book.create(
        title: "Sample Book 1",
        author_id: author.id,
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction",
        language: "English"
      )
      book2 = Book.create(
        title: "Sample Book 2",
        author_id: author.id,
        released_date: Date.new(2023, 6, 1),
        genre: "Non-Fiction",
        language: "Spanish"
      )

      result = Book.search("Sample", 1, 10, nil, Date.new(2022, 12, 31), nil, nil, nil)
      expect(result).to include(book1)
      expect(result).not_to include(book2)
    end

    it "filters books by language" do
      author = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      book1 = Book.create(
        title: "Sample Book 1",
        author_id: author.id,
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction",
        language: "English"
      )
      book2 = Book.create(
        title: "Sample Book 2",
        author_id: author.id,
        released_date: Date.new(2023, 6, 1),
        genre: "Non-Fiction",
        language: "Spanish"
      )

      result = Book.search("Sample", 1, 10, nil, nil, "English", nil, nil)
      expect(result).to include(book1)
      expect(result).not_to include(book2)
    end

    it "returns books in the specified order" do
      author = Author.create(name: "John Doe", dob: Date.new(1980, 1, 1))
      book1 = Book.create(
        title: "Sample Book 1",
        author_id: author.id,
        released_date: Date.new(2022, 1, 1),
        genre: "Fiction",
        language: "English"
      )
      book2 = Book.create(
        title: "Sample Book 2",
        author_id: author.id,
        released_date: Date.new(2021, 6, 1),
        genre: "Non-Fiction",
        language: "Spanish"
      )

      result = Book.search("Sample", 1, 10, nil, nil, nil, "title", "asc")
      expect(result.first).to eq(book1)
      expect(result.last).to eq(book2)
    end
  end
end