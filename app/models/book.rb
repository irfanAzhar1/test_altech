class Book < ApplicationRecord
  belongs_to :author
  validates_presence_of :title, :author_id, :released_date, :genre, :language
  self.per_page = 5

  def self.search_attribute data
    {
      data: data.map(&:new_attribute),
      pagination: {
        current_page: data.current_page,
        total_pages: data.total_pages,
        total_items: data.total_entries
      }
    }
  end

  def new_attribute
    {
      id: self.id,
      title: self.title,
      genre: self.genre,
      author: {
        name: self.author.name,
        age: self.author.age
      },
      released_date: self.released_date,
      language: self.language
    }
  end
  def self.search(query, page, per_page, min_realised_date, max_realised_date, language, sort_by, sort_direction)
    books = Book.joins(:author).where("authors.name LIKE :query OR books.title LIKE :query", query: "%#{query}%")
    books = books.where("released_date >= ?", min_realised_date) if min_realised_date.present?
    books = books.where("released_date <= ?", max_realised_date) if max_realised_date.present?
    books = books.where("language = ?", language) if language.present?
    books = books.order("#{sort_by} #{sort_direction}") if sort_by.present? && sort_direction.present?
    books.paginate(page: page, per_page: per_page)
  end
end
