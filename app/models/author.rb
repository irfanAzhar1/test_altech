class Author < ApplicationRecord
  has_many :books
  validates_uniqueness_of :name
  validates_presence_of :name, :dob
  self.per_page = 5

  def age
    Time.now.year - self.dob.year
  end

  def new_attribute
    {
      id: self.id,
      name: self.name,
      dob: self.dob,
      age: self.age,
      books: self.books.map do |book|
        {
          id: book.id,
          title: book.title,
          genre: book.genre,
          released_date: book.released_date,
          language: book.language
        }
      end
    }
  end

  def self.search_attribute data
    {
      data: data,
      pagination: {
        current_page: data.current_page,
        total_pages: data.total_pages,
        total_items: data.total_entries
      }
    }
  end

  def self.search(query, page, per_page, sort_by, sort_direction)
    authors = self.where("name like ?", "%#{query}%")
    authors = authors.order("#{sort_by} #{sort_direction}") if sort_by.present? && sort_direction.present?
    authors.paginate(page: page, per_page: per_page)
  end
end
