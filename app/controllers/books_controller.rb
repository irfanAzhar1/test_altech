class BooksController < ApplicationController
  before_action :set_book, only: %i[ show edit update destroy ]
  before_action :authenticate_user, except: [:index, :show]

  # GET /books or /books.json
  def index
    @books = Book.search(
      search_params[:query],
      search_params[:page],
      search_params[:per_page],
      search_params[:min_realised_date],
      search_params[:max_realised_date],
      search_params[:language],
      search_params[:sort_by],
      search_params[:sort_direction]
    ).paginate(page: params[:page], per_page: params[:per_page])

    respond_to do |format|
      format.json { render json: Book.search_attribute(@books) }
      format.html { @books = @books }
    end
  end

  # GET /books/1 or /books/1.json
  def show
    respond_to do |format|
      format.json { render json: @book.new_attribute }
      format.html { @book }
    end
  end

  # GET /books/new
  def new
    @book = Book.new
    @authors = Author.all
  end

  # GET /books/1/edit
  def edit
    @authors = Author.all
  end

  # POST /books or /books.json
  def create
    @book = Book.new(book_params)

    respond_to do |format|
      if @book.save
        MailerJob.perform_async(current_user.id, @book.id)
        format.html { redirect_to books_url, notice: "Book was successfully created." }
        format.json { render :show, status: :created, location: @book }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /books/1 or /books/1.json
  def update
    respond_to do |format|
      if @book.update(book_params)
        format.html { redirect_to book_url(@book), notice: "Book was successfully updated." }
        format.json { render :show, status: :ok, location: @book }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @book.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /books/1 or /books/1.json
  def destroy
    @book.destroy

    respond_to do |format|
      format.html { redirect_to books_url, notice: "Book was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_book
      @book = Book.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def book_params
      params.require(:book).permit(:title, :author_id, :released_date, :genre, :language)
    end

    def search_params
      params.permit(:query, :page, :per_page, :min_realised_date, :max_realised_date, :language, :sort_by, :sort_direction)
    end
end
