class AuthorsController < ApplicationController
  before_action :set_author, only: %i[ show edit update destroy ]
  before_action :authenticate_user, except: [:index, :show]

  # GET /authors or /authors.json
  def index
    @authors = Author.search(
      search_params[:query],
      search_params[:page],
      search_params[:per_page],
      search_params[:sort_by],
      search_params[:sort_direction]
    ).paginate(page: params[:page], per_page: params[:per_page])
    respond_to do |format|
      format.json {  render json: Author.search_attribute(@authors) }
      format.html { @authors = @authors }
    end
  end

  # GET /authors/1 or /authors/1.json
  def show
    respond_to do |format|
      format.json {  render json: @author.new_attribute }
      format.html { @author }
    end
  end

  # GET /authors/new
  def new
    @author = Author.new
  end

  # GET /authors/1/edit
  def edit
  end

  # POST /authors or /authors.json
  def create
    @author = Author.new(author_params)

    respond_to do |format|
      if @author.save
        format.html { redirect_to author_url(@author), notice: "Author was successfully created." }
        format.json { render :show, status: :created, location: @author }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /authors/1 or /authors/1.json
  def update
    respond_to do |format|
      if @author.update(author_params)
        format.html { redirect_to author_url(@author), notice: "Author was successfully updated." }
        format.json { render :show, status: :ok, location: @author }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @author.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /authors/1 or /authors/1.json
  def destroy
    @author.destroy

    respond_to do |format|
      format.html { redirect_to authors_url, notice: "Author was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_author
      @author = Author.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def author_params
      params.require(:author).permit(:name, :dob)
    end

    def search_params
      params.permit(:query, :page, :per_page, :sort_by, :sort_direction)
    end
end
