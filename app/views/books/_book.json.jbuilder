json.extract! book, :id, :title, :author_id, :released_date, :genre, :language, :created_at, :updated_at
json.url book_url(book, format: :json)
