class BookMailer < ApplicationMailer
  def new_book_notification(user, book)
    mail_from = ENV.fetch("SMTP_MAIL_FROM")
    @user = user
    @book = book
    mail(to: @user.email, subject: 'Notifikasi Buku Baru')
  end
end
